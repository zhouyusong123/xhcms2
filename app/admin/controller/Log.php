<?php 
/**
 *登录日志
*/

namespace app\admin\controller;

use xhadmin\service\admin\LogService;
use xhadmin\db\Log as LogDb;

class Log extends Admin {


	/*登录日志管理*/
	function index(){
		if (!$this->request->isAjax()){
			return $this->display('index');
		}else{
			$limit  = $this->request->post('limit', 0, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$where['username'] = $this->request->param('username', '', 'strip_tags,trim');

			$limit = ($page-1) * $limit.','.$limit;
			try{
				$sql = 'select a.*,b.name as group_name,c.user as username,c. name as nickname from cd_log as a inner join cd_group as b inner join cd_user as c on a.user_id = c.user_id and c.group_id= b.group_id';
				$res = LogService::pageList($sql,formatWhere($where),$limit,$orderby);
				$list = $res['list'];
			}catch(\Exception $e){
				exit($e->getMessage());
			}

			$data['rows']  = $list;
			$data['total'] = $res['count'];
			return json($data);
		}
	}

	/*数据导出*/
	function dumpData(){
		$where['username'] = $this->request->param('username', '', 'strip_tags,trim');

		$orderby = '';

		try {
			$res = LogService::dumpData(formatWhere($where),$orderby);
		} catch (\Exception $e) {
			$this->error($e->getMessage());
		}
	}

	/*删除*/
	function delete(){
		$idx =  $this->request->post('log_ids', '', 'strip_tags');
		if(!$idx) $this->error('参数错误');
		try{
			$where['log_id'] = explode(',',$idx);
			LogService::delete($where);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}



}

