<?php
/**
 * 更新字段中间件
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\admin\http\middleware;
use think\facade\Db; 

class UpField
{
	
    public function handle($request, \Closure $next)
    {	
		$data = $request->param();
		
		$typeField = \app\admin\service\FieldSetService::typeField();
        $propertyField = \app\admin\service\FieldSetService::propertyField();
		
        $typeData = $typeField[$data['type']];
        $property = $propertyField[$typeData['property']];
        if($property['decimal']){
            $property['decimal']=','.$property['decimal'];
        }else{
            $property['decimal']='';
        }
		
		$fieldInfo = Db::name('field')->where('id',$data['id'])->find();
		$info = Db::name('extend')->where('extend_id',$data['extend_id'])->find();
		
		if($data['field'] && $data['field'] <> $fieldInfo['field']){
			$fields = explode('|',$data['field']);
			$tableFileds = explode('|',$fieldInfo['field']);
			foreach($fields as $key=>$val){			
				if($val !== $v){
					$sql="ALTER TABLE ".config('database.connections.mysql.prefix').config('my.create_table_pre')."{$info['table_name']} CHANGE {$tableFileds[$key]} {$fields[$key]} {$property['name']}({$property['maxlen']}{$property['decimal']})";
					Db::execute($sql);
				}
			}
		}

		return $next($request);
    }
	
}